var judge = function judge(gdpGap, yieldGap){
  var str;

  if( gdpGap > 0 ){

    if( yieldGap > 5 ){
      str = '주식 고고씽!';
    } else if( yieldGap > 2 ){
      str = '뭐 주식이 조금 더 유리하다고 볼만함 ;)';
    } else if( yieldGap > 0 ){
      str = '별 메리트 없지만, 굳이 말하자면 주식이 좀 더 나은 정도?!';
    } else {
      str = '지금은 위험해! 주식 NO NO!';
    }

  } else {
    str = '지금은 예금할 때';
  }

  str += '\n\ngdpGap : ' + gdpGap + '\nyieldGap : ' + yieldGap;
  return str;
};

$('form').on('submit', function(e){
  var gg = parseFloat($('form input[name=gdp-gap]').val()),
      per = parseFloat($('form input[name=per]').val()),
      dir = parseFloat($('form input[name=deposit-interest-rate]').val()),

      yieldGap = (1/per)*100 - dir;

  alert(judge(gg, yieldGap));

  e.preventDefault();
});